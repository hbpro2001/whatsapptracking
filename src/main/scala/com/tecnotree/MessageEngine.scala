package com.tecnotree

import scala.collection.mutable.ArrayBuffer
import scala.util.Random

class MessageEngine {
  val numOfTotalUsers = 100
  val numOfTotalMessages = 100
  val numOfUsersInGroup = 10
  val numOfTotalGroups = 10
  val numOfTotalReceiver = 100

  def makeRandomUsers(numOfUsers: Int) = {
    var users = new ArrayBuffer[User]()
    for (i <- 1 to numOfUsers)
      users.addOne(User("user_" + Random.nextInt(numOfTotalUsers)))
    users
  }

  def makeRandomGroups(numOfGroups: Int,
                       users: List[User]) = {
    var groups = new ArrayBuffer[Group]()
    for (i <- 1 to numOfGroups) {
      groups.addOne(
        Group(
          "group_" + Random.nextInt(numOfTotalGroups),
          Random.shuffle(users).take(users.length / numOfGroups)))
    }
    groups
  }

  def makeOriginalMessages(users: List[User],
                           numOfMessages: Int) = {
    var messages = new ArrayBuffer[Message]()
    for (i <- 1 to numOfMessages) {
      messages.addOne(new Message(
        "Msg_" + Random.nextInt(numOfTotalMessages),
        Random.shuffle(users).take(1).head
      ))
    }
    messages
  }

  def initialFlow(users: List[User],
                  groups: List[Group],
                  messages: List[Message]) = {
    var messageRoutes = new ArrayBuffer[MessageRoute]()
    for (i <- 0 to messages.length - 1) {
      val messageRoute = MessageRoute(new ArrayBuffer[MessageEdge]())
      val originator = messages(i).Originator
      for (j <- 0 to groups.length - 1) {
        if (groups(j).Users.exists(i => i.Id == originator.Id)) {
          val r = Random.nextInt(2)
          if (r == 0)
            for (receiver <- groups(j).Users)
              if (receiver.Id != originator.Id)
                messageRoute.MessageList.addOne(
                  MessageEdge(
                    messages(i),
                    originator,
                    receiver)
                )
        }
      }
      for (user <- Random.shuffle(users).take(Random.nextInt(users.length))) {
        if (user.Id != originator.Id)
          messageRoute.MessageList.addOne(
            MessageEdge(
              messages(i),
              originator,
              user)
          )
      }
      messageRoutes.addOne(messageRoute)
    }
    messageRoutes
  }

  def removeDuplicatedMessageEdges(messages: List[MessageEdge]) = {
    var uniqueMessages = ArrayBuffer[MessageEdge]()
    for (msgEdge <- messages)
      if (uniqueMessages.filter(msg => (
        msg.Message.Originator.Id == msgEdge.Message.Originator.Id
          && msg.Message.Id == msgEdge.Message.Id
          && msg.Receiver.Id == msgEdge.Receiver.Id
          && msg.Sender.Id == msgEdge.Sender.Id)).length < 1)
        uniqueMessages.addOne(msgEdge)

    uniqueMessages
  }

  def forward(messageEdge: MessageEdge,
              messageRepo: ArrayBuffer[MessageEdge],
              users: List[User],
              n: Int): Unit = {
    if (n < 1) return;
    for (user <- users) {
      if (user.Id != messageEdge.Receiver.Id)
        if (Random.nextInt(10) > 7) {
          val newMsg = MessageEdge(
            messageEdge.Message,
            messageEdge.Receiver,
            user
          )
          messageRepo.addOne(newMsg)
          forward(newMsg, messageRepo, users, n - 1)
        }

    }
  }

}
