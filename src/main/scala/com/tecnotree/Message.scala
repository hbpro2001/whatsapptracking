package com.tecnotree

case class Message(var Id: String = null,
                   var Originator: User = null)
