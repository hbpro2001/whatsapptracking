package com.tecnotree

case class MessageEdge(var Message : Message = null,
                       var Sender: User = null,
                       var Receiver: User = null)
