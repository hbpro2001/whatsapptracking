package com.tecnotree

import java.io.{File, PrintWriter, StringWriter}

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule

import scala.collection.mutable.ArrayBuffer

object Driver {
  def main(args: Array[String]): Unit = {

    val engine = new MessageEngine
    val users = engine.makeRandomUsers(12)
    println(users)
    val messages = engine.makeOriginalMessages(users.toList, 2)
    println(messages)
    val groups = engine.makeRandomGroups(2, users.toList)
    println(groups)
    val msgRoutes = engine.initialFlow(users.toList, groups.toList, messages.toList)

    msgRoutes.foreach(i => println(i.MessageList.size + ": " + i))
    msgRoutes.foreach(msgRoute => {
      val v = engine.removeDuplicatedMessageEdges(
        msgRoute.MessageList.toList
      )
      println(v.size + ": " + v)
    })
    println("++++++++++++++++++++++++FORWARD++++++++++++++++++++++++++")

    msgRoutes.foreach(msgRoute => {
      msgRoute.MessageList = engine.removeDuplicatedMessageEdges(
        msgRoute.MessageList.toList
      )
    })

    msgRoutes.foreach(
      msgRoute => {
        val messageRepo = ArrayBuffer[MessageEdge]()
          msgRoute.MessageList.foreach(msgEdge => {
            engine.forward(msgEdge, messageRepo, users.toList, 1)
          }
          )
        msgRoute.MessageList.addAll(messageRepo)
      }
    )

    msgRoutes.foreach(msgRt =>{

//      msgRt.MessageList.foreach(msgEdge => {
//        println(msgEdge.Message.Originator)
//        println(msgEdge)
//      })
      println(msgRt)
      println("----")
    })

    var graphList:ArrayBuffer[Graph]=ArrayBuffer[Graph]()
    for(msgRoute <- msgRoutes){
      val graph = new Graph
      graph.build(msgRoute)
      graphList.addOne(graph)

    }
    val mapper = new ObjectMapper()
    mapper.registerModule(DefaultScalaModule)
    val fileObject = new File("data.js" )     // Creating a file
    val printWriter = new PrintWriter(fileObject)       // Passing reference of file to the printwriter
    printWriter.write("data=\'")
    val out = new StringWriter
    mapper.writeValue(out, graphList)
    val json = out.toString()
    printWriter.write(json)
    println(json)
    printWriter.write("\'")
    printWriter.close()



  }
}
