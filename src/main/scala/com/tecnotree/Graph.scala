package com.tecnotree

import scala.collection.mutable.ArrayBuffer
import scala.util.Random

class Graph {
  var nodes: ArrayBuffer[Node] = ArrayBuffer[Node]()
  var edges: ArrayBuffer[Edge] = ArrayBuffer[Edge]()

  def build(messageRoute: MessageRoute) = {

    val userNodes = (
      messageRoute.MessageList.map(mEdge=> mEdge.Sender.Id).toList
        ++ messageRoute.MessageList.map(mEdge=> mEdge.Receiver.Id).toList)
      .distinct

    nodes.addAll(userNodes.map(u => Node(u.toString.split('_')(1).toInt, u.toString)))

    edges.addAll(
      messageRoute.MessageList.map(
        msgEdge =>
          Edge(
            msgEdge.Sender.Id.split('_')(1).toInt,
            msgEdge.Receiver.Id.split('_')(1).toInt)
      )
    )
  }
}

case class Node(var id: Int, var label: String)

case class Edge( var from: Int, var to: Int,val arrows:String="to")